﻿using System;
using System.Collections.Generic;
using Bogus;

namespace BogusTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var id = 1;
            var person = new Faker<Person>()
                .StrictMode(true)
                .RuleFor(x => x.PersonId, f => id++ )
                .RuleFor(x => x.Address, f => f.Address.FullAddress())
                .RuleFor(x => x.FullName, f => f.Name.FullName())
                .RuleFor(x => x.DateOfBirth, f => f.Date.Past(30).ToLocalTime())
                .RuleFor(x => x.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(x => x.City, f => f.Address.City())
                .RuleFor(x => x.PhoneNumber, f => f.Phone.PhoneNumber());

            
            var surveys = new List<Survey>();

            for (int i = 0; i < 2; i++)
            {
                var survey = new Faker<Survey>()
                    .StrictMode(true)
                    .RuleFor(x => x.Id, f => f.Random.Guid())
                    .RuleFor(x => x.SurveyDate, f => f.Date.Past(1))
                    .RuleFor(x => x.Country, f => f.Address.Country())
                    .RuleFor(x => x.Reason, f => f.PickRandom<Reason>())
                    .RuleFor(x => x.ResultNo, f => f.Random.Double())
                    .RuleFor(x => x.ResultYes, f => f.Random.Double())
                    // .RuleFor(x => x.Participants, f => GenerateParticipants(5));
                    .RuleFor(x => x.Participants, f => person.Generate(2));

                surveys.Add(survey.Generate());
            }
            
            var surveysToReturn = ObjectDumper.Dump(surveys, DumpStyle.Console);

            Console.WriteLine(surveysToReturn);
        }

        private static List<Person> GenerateParticipants(int count)
        {
            var participants = new List<Person>();

            for (int i = 1; i <= count; i++)
            {
                var person = new Faker<Person>()
                    .StrictMode(true)
                    .RuleFor(x => x.PersonId, f => i++ )
                    .RuleFor(x => x.Address, f => f.Address.FullAddress())
                    .RuleFor(x => x.FullName, f => f.Name.FullName())
                    .RuleFor(x => x.DateOfBirth, f => f.Date.Past(30).ToLocalTime())
                    .RuleFor(x => x.Email, (f, u) => f.Internet.Email(u.FullName))
                    .RuleFor(x => x.City, f => f.Address.City())
                    .RuleFor(x => x.PhoneNumber, f => f.Phone.PhoneNumber());

                participants.Add(person.Generate());
            }

            return participants;
        }
    }
}
