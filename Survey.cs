using System;
using System.Collections.Generic;

namespace BogusTest
{
    public class Survey
    {
        public Survey()
        {
            Participants = new List<Person>();
        }
        
        public Guid Id { get; set; }
        public DateTime SurveyDate { get; set; }
        public string Country { get; set; }
        public Reason Reason { get; set; }
        public double ResultYes { get; set; }
        public double ResultNo { get; set; }
        public List<Person> Participants { get; set; }
    }

    public class Person
    {
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
    }

    public enum Reason
    {
        GlobalWarming,
        ChinaPolitics
    }
}